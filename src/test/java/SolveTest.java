import org.junit.jupiter.api.Test;
import quadricError.QuadraticErrorCode;
import quadricError.QuadraticException;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class SolveTest {

    private static final double EPSILON = 1e-5;

    Solve solve = new Solve();

    @Test
    public void oneRootTest() throws QuadraticException {
        double [] array = solve.solve(1,-2,1);
        assertAll(
                () -> assertEquals(array[0], array[1], EPSILON),
                () -> assertEquals( 1.0, array[0], EPSILON)
        );
    }

    @Test
    public void twoRootsTest() throws QuadraticException {
        double [] array = solve.solve(1,0,-1);
        Arrays.sort(array);
        assertAll(
                () -> assertEquals(-1.0, array[0], EPSILON),
                () -> assertEquals(1.0, array[1], EPSILON)
        );
    }

    @Test
    public void negativeDiscriminantTest() {
        try {
            solve.solve(1,0,1);
        } catch (QuadraticException e) {
            assertEquals(QuadraticErrorCode.NEGATIVE_DISCRIMINANT, e.getQuadraticErrorCode());
        }
    }

    @Test
    public void wrongParameterTest() {
       try {
            solve.solve(0,0,1);
        } catch (QuadraticException e) {
            assertEquals(QuadraticErrorCode.WRONG_PARAMETER_VALUE, e.getQuadraticErrorCode());
        }
    }
}
