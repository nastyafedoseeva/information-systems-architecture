package quadricError;

public class QuadraticException extends Exception{
    private QuadraticErrorCode quadraticErrorCode;

    public QuadraticException(QuadraticErrorCode quadraticErrorCode) {
        this.quadraticErrorCode = quadraticErrorCode;
    }

    public QuadraticErrorCode getQuadraticErrorCode() {
        return quadraticErrorCode;
    }
}
