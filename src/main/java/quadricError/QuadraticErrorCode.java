package quadricError;

public enum QuadraticErrorCode {
    WRONG_PARAMETER_VALUE("Wrong parameter value"),
    NEGATIVE_DISCRIMINANT("Negative discriminant");

    private String errorString;
    QuadraticErrorCode(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
