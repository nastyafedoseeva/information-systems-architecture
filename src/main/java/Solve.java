import quadricError.QuadraticErrorCode;
import quadricError.QuadraticException;

public class Solve {

    private static final double EPSILON = 1e-5;

    public double[] solve(double a, double b, double c) throws QuadraticException {

        if (Math.abs(a) < EPSILON) {
            throw new QuadraticException(QuadraticErrorCode.WRONG_PARAMETER_VALUE);
        }

        double d = b * b - 4 * a * c;

        if (d < -EPSILON) {
            throw new QuadraticException(QuadraticErrorCode.NEGATIVE_DISCRIMINANT);
        }

        double x1 = (-b - Math.sqrt(d)) / (2 * a);
        double x2 = (-b + Math.sqrt(d)) / (2 * a);

        return new double[]{x1, x2};
    }
}
